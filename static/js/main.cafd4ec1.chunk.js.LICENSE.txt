/*
        object-assign
        (c) Sindre Sorhus
        @license MIT
        */

/*
object-assign
(c) Sindre Sorhus
@license MIT
*/

/*!
                 * Determine if an object is a Buffer
                 *
                 * @author   Feross Aboukhadijeh <https://feross.org>
                 * @license  MIT
                 */

/*!
                 * is-number <https://github.com/jonschlinkert/is-number>
                 *
                 * Copyright (c) 2014-2015, Jon Schlinkert.
                 * Licensed under the MIT License.
                 */

/*!
                Copyright (c) 2017 Jed Watson.
                Licensed under the MIT License (MIT), see
                http://jedwatson.github.io/classnames
                */

/*!
          * The buffer module from node.js, for the browser.
          *
          * @author   Feross Aboukhadijeh <http://feross.org>
          * @license  MIT
          */

/*!
        Copyright (c) 2017 Jed Watson.
        Licensed under the MIT License (MIT), see
        http://jedwatson.github.io/classnames
        */

/*! @preserve
                * numeral.js
                * version : 2.0.6
                * author : Adam Draper
                * license : MIT
                * http://adamwdraper.github.com/Numeral-js/
                */

/*! @preserve
             * numeral.js
             * version : 2.0.6
             * author : Adam Draper
             * license : MIT
             * http://adamwdraper.github.com/Numeral-js/
             */

/*! @preserve
        * numeral.js
        * version : 2.0.6
        * author : Adam Draper
        * license : MIT
        * http://adamwdraper.github.com/Numeral-js/
        */

/*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */

/*! Moment Duration Format v2.2.2
                *  https://github.com/jsmreese/moment-duration-format
                *  Date: 2018-02-16
                *
                *  Duration format plugin function for the Moment.js library
                *  http://momentjs.com/
                *
                *  Copyright 2018 John Madhavan-Reese
                *  Released under the MIT license
                */

/*! Moment Duration Format v2.2.2
        *  https://github.com/jsmreese/moment-duration-format
        *  Date: 2018-02-16
        *
        *  Duration format plugin function for the Moment.js library
        *  http://momentjs.com/
        *
        *  Copyright 2018 John Madhavan-Reese
        *  Released under the MIT license
        */

/**
           * @ag-grid-community/core - Advanced Data Grid / Data Table supporting Javascript / React / AngularJS / Web Components
           * @version v23.1.0
           * @link http://www.ag-grid.com/
           * @license MIT
           */

/**
          * ag-grid-community - Advanced Data Grid / Data Table supporting Javascript / React / AngularJS / Web Components * @version v23.1.0
          * @link http://www.ag-grid.com/
          ' * @license MIT
          */

/**
         * ag-grid-enterprise - ag-Grid Enterprise Features * @version v23.1.1
         * @link http://www.ag-grid.com/
        ' * @license Commercial
         */

/** @license React v0.19.1
 * scheduler-tracing.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/** @license React v16.12.0
         * react-is.production.min.js
         *
         * Copyright (c) Facebook, Inc. and its affiliates.
         *
         * This source code is licensed under the MIT license found in the
         * LICENSE file in the root directory of this source tree.
         */

/** @license React v16.13.1
         * react-dom-server.browser.production.min.js
         *
         * Copyright (c) Facebook, Inc. and its affiliates.
         *
         * This source code is licensed under the MIT license found in the
         * LICENSE file in the root directory of this source tree.
         */

/** @license React v16.8.6
                 * react-is.production.min.js
                 *
                 * Copyright (c) Facebook, Inc. and its affiliates.
                 *
                 * This source code is licensed under the MIT license found in the
                 * LICENSE file in the root directory of this source tree.
                 */

/*@cc_on!@*/
