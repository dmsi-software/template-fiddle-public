/*!
        Copyright (c) 2017 Jed Watson.
        Licensed under the MIT License (MIT), see
        http://jedwatson.github.io/classnames
        */

/*! @preserve
             * numeral.js
             * version : 2.0.6
             * author : Adam Draper
             * license : MIT
             * http://adamwdraper.github.com/Numeral-js/
             */

/*! @preserve
        * numeral.js
        * version : 2.0.6
        * author : Adam Draper
        * license : MIT
        * http://adamwdraper.github.com/Numeral-js/
        */

/*! Moment Duration Format v2.2.2
        *  https://github.com/jsmreese/moment-duration-format
        *  Date: 2018-02-16
        *
        *  Duration format plugin function for the Moment.js library
        *  http://momentjs.com/
        *
        *  Copyright 2018 John Madhavan-Reese
        *  Released under the MIT license
        */

/** @license React v16.12.0
         * react-is.production.min.js
         *
         * Copyright (c) Facebook, Inc. and its affiliates.
         *
         * This source code is licensed under the MIT license found in the
         * LICENSE file in the root directory of this source tree.
         */
